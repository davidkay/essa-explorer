<h1 class="center">ESSA Explorer</h1>

> Demo project to show MDE application integration. It uses Node.js/Vue.js/Vuetify.js to provide a repeatable, attractive and performant development environment.

<img src="https://img.shields.io/badge/made_with-vue.js-brightgreen.svg?longCache=true&style=for-the-badge"><br />
<img src="https://img.shields.io/badge/made_with-Node.js-green.svg?longCache=true&style=for-the-badge"><br />
<img src="https://img.shields.io/badge/made_with-Vuetify-blue.svg?longCache=true&style=for-the-badge">

## Quickstart

> Clone this repo

`git clone https://gitlab.com/davidkay/essa-explorer.git`

> Change directories and install deps

`cd essa-explorer && npm i`

## Project layout

Root Directory:

```bash
├── README.md
├── build
├── config
├── index.html
├── node_modules        # node modules
├── package-lock.json
├── package.json        # list dependencies
├── src
│   ├── App.vue         # application base vue
│   ├── assets          # webpack assets
│   ├── components      # components for pages
│   ├── data            # data files
│   ├── main.js         # application setup
│   └── router          # routing setup
├── static              # static files directory
└── test                # test directory
```

## Src directory

Almost everything happens here.

### Files

- **App.vue** - The main Vue file
- **main.js** - Vue configuration file

### Directories

- **assets/** - Static assets for webpack compilation
- **components/** - Vue single file components
- **router/** - Vue router configuration

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

.dk
